<?php

// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Você deve ter recebido uma cópia da GNU Public License junto com     |
// | esse pacote; se não, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de João Prado Maia e Pablo Martins F. Costa				  |
// | 																	  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto Bradesco: Ramon Soares						            |
// +----------------------------------------------------------------------+
?>

<!DOCTYPE HTML PUBLIC '-//W3C//Dtd HTML 4.0 transitional//EN'>
<html>
<head>
<title><?php echo $dadosboleto["identificacao"]; ?></title>
<meta http-equiv=Content-Type content=text/html charset=UTF-8>
<meta name="Generator" content="Projeto BoletoPHP - www.boletophp.com.br - Licença GPL" />
<style type=text/css>
    body {
        font: 13px Arial, Helvetica, sans-serif;
        margin: 0;
        padding: 0;
        border: none;
        /*font-size: 13px;*/
    }
    #content {
        width: 100%;
        height: auto;
        overflow: hidden;
        padding: 5px 0;
        text-align: center;
        background-color: #FFFFFF;
        color: #000000;
        float: left;
    }
    table td {
        padding: 0px;
        border-spacing: 0px;
        
    }
    .cp {
        font: bold 10px Arial;
        color: black
    }
    .ti {
        font: 9px Arial, Helvetica, sans-serif
    }
    .ld {
        font: bold 15px Arial;
        color: #000000
    }
    .ct {
        FONT: 9px "Arial Narrow";
        COLOR: #000033
    }
    .cn {
        FONT: 9px Arial;
        COLOR: black
    }
    .bc {
        font: bold 20px Arial;
        color: #000000
    }
    .ld2 {
        font: bold 12px Arial;
        color: #000000
    }
</style>
</head>

<!-- <body text=#000000 bgColor=#ffffff topMargin=0 rightMargin=0> -->
<body>
<div id=content>
<table width=666px cellspacing=0 cellpadding=0 border=0>
    <tr>
        <td valign=top class=cp>
            <div ALIGN="CENTER">
                Instruções de Impressão
            </div>
        </td>
    </tr>
    <tr>
        <td valign=top class=cp>
            <div ALIGN="left">
                <p>
                    <li>Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta (Não use modo econômico).</li>
                    <li>Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens mínimas à esquerda e à direita do formulário.</li>
                    <li>Corte na linha indicada. Não rasure, risque, fure ou dobre a região onde se encontra o código de barras.</li>
                    <li>Caso não apareça o código de barras no final, clique em F5 para atualizar esta tela.</li>
                    <li>Caso tenha problemas ao imprimir, copie a seqüencia numérica abaixo e pague no caixa eletrônico ou no internet banking:</li>
                    <br><br>
                    <span class="ld2">
                        &nbsp;&nbsp;&nbsp;&nbsp;Linha Digitável: &nbsp;<?php echo $dadosboleto["linha_digitavel"]?>
                        <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;Valor: &nbsp;&nbsp;R$ <?php echo $dadosboleto["valor_boleto"]?>
                        <br>
                    </span>
                </p>
            </div>
        </td>
    </tr>
</table>
<br>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tbody>
        <tr>
            <td class=ct width=666px>
                <img height=1px src=<?php echo ('media/boleto') ?>/6.png width=665px border=0>
            </td>
        </tr>
        <tr>
            <td class=ct width=666px>
                <div align=right>
                    <b class=cp>
                        Recibo do Sacado
                    </b>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<table width=666px cellspacing=5 cellpadding=0 border=0>
    <tr>
        <td width=41px></td>
    </tr>
</table>

<table width=666px cellspacing=5 cellpadding=0 border=0 align=Default>
    <tr>
        <td width=41px><IMG SRC="<?php echo ('media/boleto') ?>/logo_pb_acsp.jpg"></td>
        <td class=ti width=455px>
            <?php echo $dadosboleto["identificacao"]; ?>
            <?php echo isset($dadosboleto["cpf_cnpj"]) ? "<br>".$dadosboleto["cpf_cnpj"] : '' ?>
            <br>
            <?php echo $dadosboleto["endereco"]; ?>
            <br>
            <?php echo $dadosboleto["cidade_uf"]; ?>
            <br>
        </td>
        <td align=RIGHT width=150px class=ti>&nbsp;</td>
    </tr>
</table>
<br>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tr>
        <td class=cp width=150px>
            <span class="campo">
                <img src="<?php echo ('media/boleto') ?>/logobradesco.jpg" width="150px" height="40px" border=0>
            </span>
        </td>
        <td width=3px valign=bottom>
            <img height=22px src=<?php echo ('media/boleto') ?>/3.png width=2px border=0>
        </td>
        <td class=cpt width=58px valign=bottom>
            <div align=center>
                <font class=bc>
                    <?php echo $dadosboleto["codigo_banco_com_dv"]?>
                </font>
            </div>
        </td>
        <td width=3px valign=bottom>
            <img height=22px src=<?php echo ('media/boleto') ?>/3.png width=2px border=0>
        </td>
        <td class=ld align=right width=453px valign=bottom>
            <span class=ld>
                <span class="campotitulo">
                    <?php echo $dadosboleto["linha_digitavel"]?>
                </span>
            </span>
        </td>
    </tr>
    <tbody>
        <tr>
            <td colspan=5>
                <img height=2px src=<?php echo ('media/boleto') ?>/2.png width=666px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0px cellpadding=0px border=0px>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0px>
            </td>
            <td class=ct valign=top width=298px height=13px>Cedente</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0px>
            </td>
            <td class=ct valign=top width=126px height=13px>
                Agência/Código do Cedente
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0px>
            </td>
            <td class=ct valign=top width=34px height=13px>Espécie</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0px>
            </td>
            <td class=ct valign=top width=53px height=13px>Quantidade</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0px>
            </td>
            <td class=ct valign=top width=120px height=13px>
                Nosso número
            </td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=298px height=12px>
                <span class="campo"><?php echo $dadosboleto["cedente"]; ?></span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=126px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["agencia_codigo"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top  width=34px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["especie"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top  width=53px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["quantidade"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=120px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["nosso_numero"] ?>
                </span>
            </td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=298px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=298px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=126px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=126px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=34px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=34px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=53px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=53px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=120px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=120px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=192px height=13px>
                Número do documento
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=132px height=13px>CPF/CNPJ</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=134px height=13px>Vencimento</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=180px height=13px>Valor documento</td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["numero_documento"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=132px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["cpf_cnpj"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=134px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["data_vencimento"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=180px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["valor_boleto"]?>
                </span>
            </td>
        </tr>
        <tr>
            <td valign=top height=1px colspan="8">
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=666px border=0>
            </td>
            <!-- <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=113px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=113px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=72px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=72px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=132px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=132px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=134px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=134px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td> -->
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=113px height=13px>
                (-) Desconto / Abatimentos
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=112px height=13px>
                (-) Outras deduções
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=113px height=13px>
                (+) Mora / Multa
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=113px height=13px>
                (+) Outros acréscimos
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=180px height=13px>
                (=) Valor cobrado
            </td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=113px height=12px></td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=112px height=12px></td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=113px height=12px></td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=113px height=12px></td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=180px height=12px></td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=113px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=113px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=112px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=112px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=113px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=113px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=113px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=113px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=659px height=13px>Sacado</td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=659px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["sacado"]?>
                </span>
            </td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=659px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=659px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct  width=7px height=12px></td>
            <td class=ct  width=564px >Demonstrativo</td>
            <td class=ct  width=7px height=12px></td>
            <td class=ct  width=88px >Autenticação mecânica</td>
        </tr>
        <tr>
            <td  width=7px ></td><td class=cp width=564px >
                <span class="campo">
                    <?php echo $dadosboleto["demonstrativo1"]?>
                    <br>
                    <?php echo $dadosboleto["demonstrativo2"]?>
                    <br>
                    <?php echo $dadosboleto["demonstrativo3"]?>
                    <br>
                </span>
            </td>
            <td  width=7px ></td>
            <td  width=88px ></td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tbody>
        <tr>
            <td width=7px></td>
            <td  width=500px class=cp>
                <br><br><br>
            </td>
            <td width=159px></td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tr><td class=ct width=666px></td></tr>
    <tbody>
        <tr>
            <td class=ct width=666px>
                <div align=right>Corte na linha pontilhada</div>
            </td>
        </tr>
        <tr>
            <td class=ct width=666px>
                <img height=1px src=<?php echo ('media/boleto') ?>/6.png width=665px border=0>
            </td>
        </tr>
    </tbody>
</table>
<br>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tr>
        <td class=cp width=150px>
            <span class="campo">
                <img src="<?php echo ('media/boleto') ?>/logobradesco.jpg" width="150px" height="40px" border=0>
            </span>
        </td>
        <td width=3px valign=bottom>
            <img height=22px src=<?php echo ('media/boleto') ?>/3.png width=2px border=0>
        </td>
        <td class=cpt width=58px valign=bottom>
            <div align=center>
                <font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"]?></font>
            </div>
        </td>
        <td width=3px valign=bottom>
            <img height=22px src=<?php echo ('media/boleto') ?>/3.png width=2px border=0>
        </td>
        <td class=ld align=right width=453px valign=bottom>
            <span class=ld>
                <span class="campotitulo">
                    <?php echo $dadosboleto["linha_digitavel"]?>
                </span>
            </span>
        </td>
    </tr>
    <tbody>
        <tr>
            <td colspan=5>
                <img height=2px src=<?php echo ('media/boleto') ?>/2.png width=666px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=472px height=13px>
                Local de pagamento
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=180px height=13px>Vencimento</td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=472px height=12px>PAGAVEL PREFERENCIALMENTE EM QUALQUER AGENCIA BRADESCO</td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=180px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["data_vencimento"]?>
                </span>
            </td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=472px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=472px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=472px height=13px>Cedente</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=180px height=13px>Agência/Código cedente</td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=472px height=12px>
                <span class="campo"><?php echo $dadosboleto["cedente"]?></span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=180px height=12px>
                <span class="campo"><?php echo $dadosboleto["agencia_codigo"]?></span>
            </td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=472px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=472px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>                
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=113px height=13px>Data do documento</td>
            <td class=ct valign=top width=7px height=13px> 
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=153px height=13px>
                N<u>o</u>documento
            </td>
            <td class=ct valign=top width=7px height=13px> 
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=62px height=13px>
                Espécie doc.
            </td>
            <td class=ct valign=top width=7px height=13px> 
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=34px height=13px>Aceite</td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=82px height=13px>
                Data processamento
            </td>
            <td class=ct valign=top width=7px height=13px> 
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=180px height=13px>
                Nosso número
            </td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top  width=113px height=12px>
                <div align=left>
                    <span class="campo">
                        <?php echo $dadosboleto["data_documento"]?>
                    </span>
                </div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=153px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["numero_documento"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top  width=62px height=12px>
                <div align=left>
                    <span class="campo">
                        <?php echo $dadosboleto["especie_doc"]?>
                    </span>
                </div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top  width=34px height=12px>
                <div align=left>
                    <span class="campo">
                        <?php echo $dadosboleto["aceite"]?>
                    </span>
                </div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top  width=82px height=12px>
                <div align=left>
                    <span class="campo">
                        <?php echo $dadosboleto["data_processamento"]?>
                    </span>
                </div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=180px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["nosso_numero"]?>
                </span>
            </td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=113px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=113px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=153px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=153px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=62px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=62px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=34px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=34px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=82px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=82px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td>
        </tr>
    </tbody>
</table>
    
<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px> 
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=56px height=13px>Uso do banco</td>
            <td class=ct valign=top height=13px width=7px> 
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=83px height=13px>Carteira</td>
            <td class=ct valign=top height=13px width=7px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=53px height=13px>Espécie</td>
            <td class=ct valign=top height=13px width=7px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=123px height=13px>Quantidade</td>
            <td class=ct valign=top height=13px width=7px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=72px height=13px>
                Valor Documento
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=169px height=13px>
                (=) Valor documento
            </td>
        </tr>
        <tr> 
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td valign=top class=cp width=56px height=12px>
                <div align=left></div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=83px>
                <div align=left> 
                    <span class="campo"><?php echo $dadosboleto["carteira"]?></span>
                </div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=53px>
                <div align=left>
                    <span class="campo"><?php echo $dadosboleto["especie"]?></span>
                </div>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=123px>
                <span class="campo"><?php echo $dadosboleto["quantidade"]?></span>
            </td>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=72px>
                <span class="campo">
                    <?php echo $dadosboleto["valor_unitario"]?>
                </span>
            </td>
            <td class=cp valign=top width=7px height=12px> 
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top align=right width=169px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["valor_boleto"]?>
                </span>
            </td>
        </tr>
        <tr>
            <td valign=top width=666px colspan=12 height=1px> 
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=666px border=0>
            </td>
            <!-- <td valign=top width=7px height=1px> 
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=75px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=31px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=31px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=83px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=83px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=53px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=53px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=123px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=123px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=72px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=72px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td>
        </tr> -->
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tbody>
        <tr>
            <td align=right width=10px>
                <table cellspacing=0 cellpadding=0 border=0 align=left>
                    <tbody>
                        <tr> 
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=1px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td valign=top width=468px rowspan=5>
                <font class=ct>Instruções (Texto de responsabilidade do cedente)</font>
                <br><br>
                <span class=cp> 
                    <font class=campo>
                        <?php echo $dadosboleto["instrucoes1"]; ?>
                        <br>
                        <?php echo $dadosboleto["instrucoes2"]; ?>
                        <br>
                        <?php echo $dadosboleto["instrucoes3"]; ?>
                        <br>
                        <?php echo $dadosboleto["instrucoes4"]; ?>
                    </font>
                    <br><br>
                </span>
            </td>
            <td align=right width=188px>
                <table cellspacing=0 cellpadding=0 border=0>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=ct valign=top width=180px height=13px>
                                (-) Desconto / Abatimentos
                            </td>
                        </tr>
                        <tr> 
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=cp valign=top align=right width=180px height=12px></td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
                            </td>
                            <td valign=top width=180px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align=right width=10px>
                <table cellspacing=0 cellpadding=0 border=0 align=left>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=1px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td align=right width=188px>
                <table cellspacing=0 cellpadding=0 border=0>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=ct valign=top width=180px height=13px>(-) Outras deduções</td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px> 
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=cp valign=top align=right width=180px height=12px></td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
                            </td>
                            <td valign=top width=180px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align=right width=10px>
                <table cellspacing=0 cellpadding=0 border=0 align=left>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=1px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td align=right width=188px>
                <table cellspacing=0 cellpadding=0 border=0>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=ct valign=top width=180px height=13px>(+) Mora / Multa</td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=cp valign=top align=right width=180px height=12px></td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px> 
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
                            </td>
                            <td valign=top width=180px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align=right width=10px>
                <table cellspacing=0 cellpadding=0 border=0 align=left>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=1px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td align=right width=188px>
                <table cellspacing=0 cellpadding=0 border=0>
                    <tbody>
                        <tr> 
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=ct valign=top width=180px height=13px>(+) Outros acréscimos</td>
                        </tr>
                        <tr> 
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=cp valign=top align=right width=180px height=12px></td>
                        </tr>
                        <tr>
                            <td valign=top width=7px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
                            </td>
                            <td valign=top width=180px height=1px>
                                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align=right width=10px>
                <table cellspacing=0 cellpadding=0 border=0 align=left>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td align=right width=188px>
                <table cellspacing=0 cellpadding=0 border=0>
                    <tbody>
                        <tr>
                            <td class=ct valign=top width=7px height=13px>
                                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=ct valign=top width=180px height=13px>(=) Valor cobrado</td>
                        </tr>
                        <tr>
                            <td class=cp valign=top width=7px height=12px>
                                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
                            </td>
                            <td class=cp valign=top align=right width=180px height=12px></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666px border=0>
    <tbody>
        <tr>
            <td valign=top width=666px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=666px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=659px height=13px>Sacado</td>
        </tr>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=659px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["sacado"]?>
                </span>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=cp valign=top width=7px height=12px>
                <img height=12px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=659px height=12px>
                <span class="campo">
                    <?php echo $dadosboleto["endereco1"]?>
                </span>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
        <tr>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=cp valign=top width=472px height=13px>
                <span class="campo">
                    <?php echo $dadosboleto["endereco2"]?>
                </span>
            </td>
            <td class=ct valign=top width=7px height=13px>
                <img height=13px src=<?php echo ('media/boleto') ?>/1.png width=1px border=0>
            </td>
            <td class=ct valign=top width=180px height=13px>Cód. baixa</td>
        </tr>
        <tr>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=472px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=472px border=0>
            </td>
            <td valign=top width=7px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=7px border=0>
            </td>
            <td valign=top width=180px height=1px>
                <img height=1px src=<?php echo ('media/boleto') ?>/2.png width=180px border=0>
            </td>
        </tr>
    </tbody>
</table>

<table cellSpacing=0 cellPadding=0 border=0 width=666px>
    <tbody>
        <tr>
            <td class=ct  width=7px height=12px></td>
            <td class=ct  width=409px >Sacador/Avalista</td>
            <td class=ct  width=250px >
                <div align=right>Autenticação mecânica - <b class=cp>Ficha de Compensação</b></div>
            </td>
        </tr>
        <tr>
            <td class=ct  colspan=3 ></td>
        </tr>
    </tbody>
</table>

<table cellSpacing=0 cellPadding=0 width=666px border=0>
    <tbody>
        <tr>
            <td vAlign=bottom align=left height=50px><?php fbarcode($dadosboleto["codigo_barras"]); ?></td>
        </tr>
    </tbody>
</table>

<table cellSpacing=0 cellPadding=0 width=666px border=0>
    <tr>
        <td class=ct width=666px></td>
    </tr>
    <tbody>
        <tr>
            <td class=ct width=666px>
                <div align=right>Corte na linha pontilhada</div>
            </td>
        </tr>
        <tr>
            <td class=ct width=666px>
                <img height=1px src=<?php echo ('media/boleto') ?>/6.png width=665px border=0>
            </td>
        </tr>
    </tbody>
</table>

</div>

</body>

</html>
