<?php

namespace acsp\boleto;

class bradesco {
    use \doctrine\Dashes\Model;
    
    public function boleto_bradesco($dados_cliente=null, $dados_empresa=null, $dados_boleto=null, $valores_boleto=null, $layout = true)
    {
        // DADOS DO BOLETO PARA O SEU CLIENTE
//        $dias_de_prazo_para_pagamento = 5;
        $taxa_boleto = 0;
        $data_venc = $dados_boleto['data_vencimento'];  // Prazo de X dias OU informe data: "13/04/2006"; 
        $valor_cobrado = str_replace(",", ".", $valores_boleto['valor_cobrado']); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal

        $valor_boleto = number_format($valor_cobrado + $taxa_boleto, 2, ',', '');

        $dadosboleto["nosso_numero"] = $dados_boleto['nosso_numero'];  // Nosso numero sem o DV - REGRA: Máximo de 11 caracteres!
        $dadosboleto["numero_documento"] = $dados_boleto["numero"];	// Num do pedido ou do documento = Nosso numero

        $dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        $dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
        $dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
        $dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

        // DADOS DO SEU CLIENTE
        $dadosboleto["sacado"] = $dados_cliente['sacado']; // Nome do seu cliente
        $dadosboleto["endereco1"] = $dados_cliente['endereco1']; // Endereço do seu Cliente
        $dadosboleto["endereco2"] = $dados_cliente['endereco2']; // Cidade - Estado -  CEP: 00000-000

        // INFORMACOES PARA O CLIENTE
        $dadosboleto["demonstrativo1"] = '';//$dados_boleto['produtos'];
        $dadosboleto["demonstrativo2"] = '';//$dados_boleto['info_produtos']; //  . "<br>Taxa bancária - R$ " . number_format($taxa_boleto, 2, ',', '')
        $dadosboleto["demonstrativo3"] = '';
        $dadosboleto["instrucoes1"] = "MORA DIA R$ " . number_format($valores_boleto['mora_dia'], 2, ',', ''); // - Sr. Caixa, cobrar multa de 2% após o vencimento
        $dadosboleto["instrucoes2"] = ""; // - Receber até 10 dias após o vencimento
        $dadosboleto["instrucoes3"] = $dados_boleto['informativo'];
        $dadosboleto["instrucoes4"] = ""; // &nbsp; Emitido pelo sistema Projeto BoletoPhp - www.boletophp.com.br

        // DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
        $dadosboleto["quantidade"] = "001";
        $dadosboleto["valor_unitario"] = $valor_boleto;
        $dadosboleto["aceite"] = "";
        $dadosboleto["especie"] = $valores_boleto['especie'];
        $dadosboleto["especie_doc"] = "REC";

        // ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
        // verificar esses dados junto ao financeiro

        // DADOS DA SUA CONTA - Bradesco
        $dadosboleto["agencia"] = substr($dados_empresa['agencia'], 1); // Num da agencia, sem digito
        $dadosboleto["agencia_dv"] = $dados_empresa['agencia_dv']; // Digito do Num da agencia
        $dadosboleto["conta"] = $dados_empresa['conta']; 	// Num da conta, sem digito
        $dadosboleto["conta_dv"] = $dados_empresa['conta_dv']; 	// Digito do Num da conta

        // DADOS PERSONALIZADOS - Bradesco
        $dadosboleto["conta_cedente"] = $dados_empresa['conta']; // ContaCedente do Cliente, sem digito (Somente Números)
        $dadosboleto["conta_cedente_dv"] = $dados_empresa['conta_dv']; // Digito da ContaCedente do Cliente
        $dadosboleto["carteira"] = $dados_empresa['carteira'];  // Código da Carteira: pode ser 06 ou 03

        // SEUS DADOS
        $dadosboleto["identificacao"] = "Associação Comercial de São Paulo"; // BoletoPhp - Código Aberto de Sistema de Boletos
        $dadosboleto["cpf_cnpj"] = "60.524.550/0001-31";
        $dadosboleto["endereco"] = "Rua Boa Vista, nº 51 / 9º andar"; // Coloque o endereço da sua empresa aqui
        $dadosboleto["cidade_uf"] = "São Paulo / SP"; // Cidade / Estado
        $dadosboleto["cedente"] = "ASSOCIACAO COMERCIAL DE SAO PAULO"; // Coloque a Razão Social da sua empresa aqui
echo "<pre>"; print_r($dadosboleto); exit;
        // NÃO ALTERAR!
        require_once "funcoes_bradesco.php"; 
        require "dados_bradesco.php";
        require "layout_bradesco.php";
    }
}